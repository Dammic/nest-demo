import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { BlogService } from './blog.service';
import { PrismaService } from '../prisma.service';
import { BlogController } from './blog.controller';
import { AuthenticationMiddleware } from 'src/common/authentication.middleware';

@Module({
  imports: [],
  providers: [BlogService, PrismaService],
  controllers: [BlogController],
})

export class BlogModule implements NestModule {
  configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
    consumer
      .apply(AuthenticationMiddleware)
      .forRoutes(
        { method: RequestMethod.POST, path: '/blog/post' },
        { method: RequestMethod.PUT, path: '/blog/edit' },
        { method: RequestMethod.DELETE, path: '/blog/delete' },
      );
  }
}
