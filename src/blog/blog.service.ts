import { Injectable } from '@nestjs/common';
import { Post, Prisma } from '@prisma/client';
import { PrismaService } from '../prisma.service';

@Injectable()
export class BlogService {
  constructor(private prisma: PrismaService) {}

  async addPost(data: Prisma.PostCreateInput): Promise<Post> {
    const newPost = await this.prisma.post.create({ data });
    return newPost;
  }

  async getPost(postID): Promise<Post> {
    const post = await this.prisma.post.findUnique({ where: { id: postID } });
    return post;
  }

  async getPosts(): Promise<Post[]> {
    const posts = await this.prisma.post.findMany();
    return posts;
  }

  async editPost(postID, data: Prisma.PostUpdateInput): Promise<Post> {
    const editedPost = await this.prisma.post.update({
      where: { id: postID },
      data,
    });
    return editedPost;
  }

  async deletePost(postID): Promise<Post> {
    const deletedPost = await this.prisma.post.delete({
      where: { id: postID },
    });
    return deletedPost;
  }
}
