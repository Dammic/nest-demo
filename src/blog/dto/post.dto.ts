export class CreatePostDto {
  title: string;
  description: string;
  body: string;
  author: string;
  date_posted: string;
}

export class UpdatePostDto {
  title: string;
  description: string;
  body: string;
  author: string;
  date_posted: string;
}

export class PostDto {
  id: string;
  title: string;
  description: string;
  body: string;
  author: string;
  date_posted: string;
}
