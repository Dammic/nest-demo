import { Post, Prisma } from '@prisma/client';
import { PrismaService } from '../prisma.service';
export declare class BlogService {
    private prisma;
    constructor(prisma: PrismaService);
    addPost(data: Prisma.PostCreateInput): Promise<Post>;
    getPost(postID: any): Promise<Post>;
    getPosts(): Promise<Post[]>;
    editPost(postID: any, data: Prisma.PostUpdateInput): Promise<Post>;
    deletePost(postID: any): Promise<Post>;
}
