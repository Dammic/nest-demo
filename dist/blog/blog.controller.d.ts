import { Prisma } from '@prisma/client';
import { BlogService } from './blog.service';
export declare class BlogController {
    private blogService;
    constructor(blogService: BlogService);
    addPost(res: any, data: Prisma.PostCreateInput): Promise<any>;
    getPost(res: any, postID: any): Promise<any>;
    getPosts(res: any): Promise<any>;
    editPost(res: any, postID: any, data: Prisma.PostUpdateInput): Promise<any>;
    deletePost(res: any, postID: any): Promise<any>;
}
