export declare class CreatePostDto {
    title: string;
    description: string;
    body: string;
    author: string;
    date_posted: string;
}
export declare class UpdatePostDto {
    title: string;
    description: string;
    body: string;
    author: string;
    date_posted: string;
}
export declare class PostDto {
    id: string;
    title: string;
    description: string;
    body: string;
    author: string;
    date_posted: string;
}
